package main

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"

	"Go_Gingonic_Server/product"
)

func initDB() *gorm.DB { //Gorm Abre la conexion a base de datos
	var DBHOST = "mysql"
	var DBPORT = "3306"
	var DBUSER = "root"
	var DBPASS = "test"
	var DBNAME = "test_go_gql"
	var DRIVER = "mysql"
	db, err := gorm.Open(DRIVER, fmt.Sprintf("%s:%s@tcp("+DBHOST+":"+DBPORT+")/%s", DBUSER, DBPASS, DBNAME))
	if err != nil {
		panic(err)
	}

	db.AutoMigrate(&product.Product{}) //genera las tablas dependiendo de los modelos que le pasemos

	return db
}

func main() {
	db := initDB()   //abrimos la conexion a base de datos
	defer db.Close() //a cerramos

	productAPI := initProductAPI(db) //illectamos todas las dependencias que necesitamos con wire.

	r := gin.Default() //creamos el router

	//creamos las rutas y anexamos el capturador el cual se encontrara en <nombre>API
	r.GET("/products", productAPI.FindAll)
	r.GET("/products/:id", productAPI.FindByID)
	r.POST("/products", productAPI.Create)
	r.PUT("/products/:id", productAPI.Update)
	r.DELETE("/products/:id", productAPI.Delete)

	err := r.Run(":3010") //arrancamos el servidor por el puerto indicado
	if err != nil {
		panic(err)
	}
}
